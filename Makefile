BIN = dtrack
ARCH := $(firstword $(shell uname -s))

default:
	@echo "Building for $(ARCH)"
	@make build-$(ARCH)

build:
ifeq ($(ARCH),Linux)
	go build -o $(BIN)
else ifeq ($(OS),Windows_NT)
	go build -o $(BIN).exe
endif

build-windows:
	@echo "Building for Windows"
	go build -o $(BIN).exe
build-linux:
	@echo "Building for Linux"
	env GOOS=linux GOARCH=amd64 go build -o $(BIN) cmd/dtrack/main.go
build-osx:
	@echo "Building for OSX"
	env GOOS=darwin GOARCH=amd64 go build -o $(BIN) cmd/dtrack/main.go

clean:
	rm -f $(BIN)
	rm -f $(BIN).exe

.PHONY: default build build-osx build-windows build-linux clean
