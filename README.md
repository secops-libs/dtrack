# dtrack

[OWASP Dependency Track](https://dependencytrack.org) API client for your security CI/CD pipeline.
See [Dependency-Track docs: Continuous Integration & Delivery](https://docs.dependencytrack.org/usage/cicd/) for use
case.

## Build & Install

Requirement: `Go 1.20+`

### Quick Install

```bash
# Linux/MacOS/Windows - console
go install gitlab.com/secops-libs/dtrack@latest

# Build from sources
git clone https://gitlab.com/secops-libs/dtrack && cd dtrack
make build

# Docker Container
TODO
```

## Features

* Fully configurable via environment variables
* Async and sync modes. In async mode dtrack-api simply sends SBOM file to DTrack API (like cURL but *in much more
  comfortable way*). Sync mode means: upload SBOM file, wait for the scan result, show it and exit with non-zero code.
  So you can break corresponding CI/CD job to make developers pay attention to findings
* You can filter the results. With Sync mode enabled show result and fail an audit __if the results include a
  vulnerability with a severity of specified level or higher__. Severity levels are: critical, high, medium, low, info,
  unassigned
* Auto creation of projects. With this feature you can configure SCA (with dtrack-api) step globally for your CI/CD and
  it will create project, e.g. with name from environment variable like `$CI_PROJECT_NAME`. So you don't need to
  configure it manually for each project

### Sample output

```bash
$ cyclonedx-bom -o bom.xml
$ dtrack -s -g high

SBOM file is successfully uploaded to DTrack API. Result token is 12345f5e-4ccb-45fe-b8fd-1234a8bf0081

2 vulnerabilities found!

 > HIGH: Arbitrary File Write
   Component: adm-zip 0.4.7
   More info: https://dtrack/vulnerability/?source=NPM&vulnId=994

 > CRITICAL: Prototype Pollution
   Component: handlebars 4.0.11
   More info: https://dtrack/vulnerability/?source=NPM&vulnId=755
```

### Help output

```bash
Usage of program:
  -a    Auto create project with projectName if it does not exist. Environment variable is DTRACK_AUTO_CREATE_PROJECT
  -g string
        With Sync mode enabled show result and fail an audit if the results include a vulnerability with a severity of specified level or higher. Severity levels are: critical, high, medium, low, info, unassigned. Environment variable is DTRACK_SEVERITY_FILTER
  -i string
        Target SBOM file* (default "bom.xml")
  -k string
        API Key*. Environment variable is DTRACK_PLATFORM_KEY
  -n string
        Project name. It is used for auto creation of project. See option AutoCreateProject for details. Environment variable is DTRACK_PROJECT_NAME
  -p string
        Project ID. Environment variable is DTRACK_PROJECT_ID
  -s    Sync mode enabled. That means: upload SBOM file, wait for scan result, show it and exit with non-zero code. Environment variable is DTRACK_SYNC_MODE
  -t int
        Max timeout in second for polling API for project findings (default 30)
  -u string
        API URL*. Environment variable is DTRACK_PLATFORM_URL
  -v string
        Project version. It is used for auto creation of project. See option AutoCreateProject for details. Environment variable is DTRACK_PROJECT_VERSION
```

### Usages: Sample upload

Show how you can upload SBoM specification file to your self-deployed dependency-track.
Use flag `-a` create new project. Next runnings no needs to delete `-a` flag, update will be correct anyway.

```bash
dtrack -a -i sbom.xml -u {https://dependencytrack.domain.tld} -k {API_KEY} -n [PROJECT_NAME] -v [VERSION]
```

### Integration in CI/CD

Available CI/CD Variables form ENV

| Variable | Type | Example | Note |
| --- | --- | --- | --- |
| DTRACK_PROJECT_ID | string | `empty` | Generated UUID (see flag `-a`) |
| DTRACK_PROJECT_NAME | string | “My Awesome Secure App” | Possible contains whitespace in name |
| DTRACK_PROJECT_VERSION | string | `1.0.0` | Your product version |
| DTRACK_PLATFORM_KEY | string | `oUtoEV--------hom594` | Dependency Track API-Key |
| DTRACK_PLATFORM_URL | string | "https\:\/\/dtrack.tld\/api" | Link to API service platform Dependency Track |
| DTRACK_SEVERITY_FILTER | int | 3   | 6 Levels [CRITICAL .. LOWER] |
| DTRACK_SYNC_MODE | bool | `empty` | Return result in pipeline? [Can be empty] |
| DTRACK_AUTO_CREATE_PROJECT | bool | `empty` | For first run create, for next find and update. [Can be empty] |
